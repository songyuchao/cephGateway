#!/bin/bash
#Author: jack
#Date & Time: 2016-09-26
#Description:
#Version:0.0.1
#

aptgetsrc=${PWD}'/../apt-src/'
installpippath=${PWD}'/../apt-src/'
projectfile=${PWD}'/../../../src/'
conf=${PWD}'/../conf/'
echo $aptgetsrc
cp /etc/default/locale /etc/default/locale.back 
echo "LANG='en_US.UTF-8'\nLANGUAGE='en_US.en'" > /etc/default/locale 
mv /etc/apt/sources.list /etc/apt/sources.list.back
cp $aptgetsrc'sources.list' /etc/apt/ -rf
sudo apt-key add $aptgetsrc"autobuild.asc"
sudo apt-key add $aptgetsrc"jessie-stable-release.asc"
sudo apt-key add $aptgetsrc"release.asc"
sudo apt-get update
sudo apt-get install -y python-ceph ceph-mon 
sudo apt-get install -y  nginx supervisor
mkdir /opt/objectStorageGateway/
cp $projectfile* /opt/objectStorageGateway -r
cp $conf'supervisor_conf.conf' /etc/supervisor/conf.d/ceph.conf
cp $conf'nginx_conf.conf' /etc/nginx/sites-enabled/ceph.conf
cp $conf'gateway.conf' /opt/objectStorageGateway/
cp $conf'ceph.conf' /opt/objectStorageGateway/
cp $conf'ceph.keyring/' /opt/objectStorageGateway/
rm /etc/nginx/sites-enabled/default
mkdir ~/.pip/
echo '[global]\nindex-url = https://pypi.tuna.tsinghua.edu.cn/simple'> ~/.pip/pip.conf
/usr/bin/python ${PWD}'/../get-pip.py'
pip install ${PWD}/../*.whl
         
