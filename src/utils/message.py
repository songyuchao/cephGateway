# encoding:utf-8

class Message(object):
    def __init__(self, message):
        self.message = eval(message)

    def is_init(self):
        return self.message.get('type') in ('init',)

    def __getattr__(self, item):
        return self.message.get(item)