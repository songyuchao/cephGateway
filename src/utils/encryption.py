# -*- coding:utf-8 -*-
# !/usr/bin/env python
# @author: rui.xu
# 这里使用pycrypto‎库
# 按照方法:easy_install pycrypto‎
__author__ = 'JackSong'

from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex
from tornado import options
import os
import datetime


class Prpcrypt(object):
    def __init__(self):
        options.define("password_key", default='lwkj201512345678', type=str, help='encrytion password key')
        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        options.options.parse_config_file(''.join((path, "/webssh.conf",)))
        options.parse_command_line()
        self.__key = options.options.password_key
        self.mode = AES.MODE_CBC
        self.iv = b'0000000000000000'

    # 加密函数，如果text不足16位就用空格补足为16位，
    # 如果大于16当时不是16的倍数，那就补足为16的倍数。
    def encrypt(self, text):
        cryptor = AES.new(self.__key, self.mode, self.iv)
        # 这里密钥key 长度必须为16（AES-128）,
        # 24（AES-192）,或者32 （AES-256）Bytes 长度
        # 目前AES-128 足够目前使用
        length = 16
        count = len(text)
        if count < length:
            add = (length - count)
            # \0 backspace
            text = ''.join((text, ('\0' * add),))
        elif count > length:
            add = (length - (count % length))
            text = ''.join((text, ('\0' * add),))
        self.ciphertext = cryptor.encrypt(text)
        # 因为AES加密时候得到的字符串不一定是ascii字符集的，输出到终端或者保存时候可能存在问题
        # 所以这里统一把加密后的字符串转化为16进制字符串
        return b2a_hex(self.ciphertext)

    # 解密后，去掉补足的空格用strip() 去掉
    def decrypt(self, text):
        cryptor = AES.new(self.__key, self.mode, self.iv)
        plain_text = cryptor.decrypt(a2b_hex(text)).decode()
        return plain_text.rstrip('\0')

