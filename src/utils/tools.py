__author__ = 'JackSong'

import re
import sys,platform


_PLATFORM = sys.platform


def check_ip(ip):
    pattern = re.compile(
        '^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
    return True if pattern.match(ip) else False


def check_port(port):
    if port and port.isdigit():
        iport = int(port)
        return 0 < iport < 65536
    return False


class Platform(object):

    @staticmethod
    def detail():
        return _PLATFORM

    @staticmethod
    def is_win():
        return _PLATFORM.startswith('win')

    @staticmethod
    def is_linux():
        return _PLATFORM.startswith('linux')

    @staticmethod
    def is_mac():
        return _PLATFORM.startswith('darwin')

class PrpcryptInitData(object):
    def __init__(self,**kwargs):
        from utils.encryption import Prpcrypt
        self.kwargs = kwargs
        self.crypt = Prpcrypt()

    @property
    def password(self):
        import base64
        return base64.b64encode(''.join(('ZNZ', self.hostname, 'cloud',)))

    def __getattr__(self, item):

        get = self.kwargs.get(item)
        if get:
            return self.crypt.decrypt(get)
        else:
            return None
def check_python_version(ver):
    var =ver.split('.')
    version=platform.python_version_tuple()
    new_ver= []
    for num in range(0,len(var)):
        new_ver.append(version[num])
    if int(''.join(var)) <= int(''.join(new_ver)):
        return True
    else:
        return False
