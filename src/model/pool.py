# encoding:utf-8
from sqlalchemy import Column, ForeignKey, Table, Boolean
from sqlalchemy.types import CHAR, Integer
from sqlalchemy.orm import relationship
from db import BaseModel


class Pool(BaseModel):
    __tablename__ = 'pool'
    id = Column(CHAR(36), primary_key=True)
    name = Column(CHAR(200))
    mold = Column(Integer, default=1)
    policy = Column(Integer, default=1)
    users = relationship('Pool', secondary='pool_and_user', backref='pools',cascade="save-update, merge, delete")
    html = Column(Boolean, default=True)
    _policy_dict = {1: u"私有空间", 2: u"公共读空间"}

    def __init__(self, name, mold=1, policy=1, html=True):
        import uuid
        self.name = name
        self.mold = mold
        self.policy = policy
        self.html = html
        self.id = unicode(uuid.uuid4())

    def __repr__(self):
        return '<pool {0},name: {1} >'.format(self.id, self.name)


AssociationTable = Table(
    'pool_and_user', BaseModel.metadata,
    Column('user_id', CHAR(36), ForeignKey('user.id')),
    Column('pool_id', CHAR(36), ForeignKey('pool.id'))
)
