__author__ = 'JackSong'
import ConfigParser


class Conf(object):
    def __init__(self, file_path):
        self.conf = ConfigParser.ConfigParser()
        self.conf.read(file_path)

    def __getattr__(self, item):

        class Config(object):
            def __init__(self,conf, name):
                self.name = name
                self.conf = conf

            def __getattr__(self, item):
                try:
                    val = self.conf.get(self.name, item)
                except ConfigParser.NoOptionError:
                    val = ''
                return val


        return Config(self.conf,item)
