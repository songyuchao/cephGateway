__author__ = 'JackSong'

from handlers.readWriteHandler import *
from handlers.manageHandler import *
from handlers.authHandler import *



handlers = [
    (r"^/pool/$", PoolHandler),
    (r"^/file/$", FileHandler),
    (r"^/rbd/manager/$", RBDManageHandler),
    (r"^/rbd/snap/$", RBDSnapHandler),
    (r"^/rbd/protect/$", RBDProtectHandler),
    (r"^/largefile/$", LargeFilesHandler),
    (r"^/ws/$", WSObjectUploadHandlerWeb),
    (r"^/ws/rbd/$", WSRBDHandlerWeb),
    (r"^/download/$", DownloadHandler),
    (r"^/auth/$", AuthHandler),
    (r"^/rbd/$", RBDHandler),
    (r"^/xattr/$", XattrHandler),
    (r"^/user/$", UserHandler),
    (r"^/sync/$", SyncHandler),
    (r"^/ws/sync/$", SyncObjectHandlerWeb),
]
