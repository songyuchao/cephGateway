(function($){
    var method = {
        check_support_file_api:function(){
            if(window.File && window.FileList && window.FileReader && window.Blob) {
                        return true;
            }
                 return false;
        }
    }
    $.fn.upload_file = function(data){
        function loadNext() {
         var start = currentChunk * chunkSize, end = start + chunkSize >= me.file.size ? me.file.size : start + chunkSize;
             reader.readAsDataURL(blobSlice.call(me.file, start, end));
        }

        connection = function (){
            if (window.WebSocket) {
                    me._connection = new WebSocket(endpoint);
            }
            else if (window.MozWebSocket) {
                    me._connection = MozWebSocket(endpoint);
          }
          }
       generateEndpoint = function() {
        if (window.location.protocol == 'https:') {
            var protocol = 'wss://';
            } else {
            var protocol = 'ws://';
                }
            var endpoint = protocol + window.location.host + '/ws/';
         return endpoint;
        };
        if(window.localStorage){
            console.log('This browser supports localStorage');
        }else{
            alert('This browser does NOT support localStorage');
        }
        var me = $(this),
        reader = new FileReader(),
        endpoint =generateEndpoint(),
        chunkSize  =data.chunkSize,
        spark = new SparkMD5()
        blobSlice = File.prototype.mozSlice || File.prototype.webkitSlice || File.prototype.slice
        currentChunk = 0
        me.find('input[type="file"]').bind('change',function(){
            connection()
            me.file= this.files[0]
            var input = $(this)
            $(this).after('<div class="progress" style="display: block;width: 100%;height: 20px;background-color: black;text-align: right;"><div style="display: block;width: 20%;height: 20px; background-color: red;"></div></div>')
            chunks = Math.ceil(me.file.size / chunkSize),
            reader.onload = function(e){
                data =e.target.result
                me._connection.send(data)
                currentChunk ++
                num =Math.round(currentChunk / chunks * 10000) / 100.00 + "%"
                $($(input.next()).children()[0]).css('width',num)
            }
            me._connection.onopen = function(){
                me._connection.send(JSON.stringify({"filename":me.file.name,'poolname':data.poolname,'offset':0}))
               loadNext()
            }
            me._connection.onmessage = function(e){
                if (e.data == "success"){
                    if (currentChunk < chunks) {
                        spark.appendBinary(data)
                         loadNext();
                 } else {
                    console.log("finished loading");
                    console.log(spark.end())
                    me._connection.close()
                 }
                }
            }
        })
    }
})(jQuery);
