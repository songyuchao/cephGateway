(function($){
    var defaults = {
        poolname:'test',
        chunkSize:2097152,
        url:'/ws/rbd/',
        ip:false,
    }
    var method = {
        check_support_file_api:function(){
            if(window.File && window.FileList && window.FileReader && window.Blob) {
                        return true;
            }
                 return false;
        }
    }
    $.fn.upload_rbd = function(options){
        var options=$.extend(defaults,options)
        var me = $(this),
        chunkSize  =options.chunkSize,
        currentChunk = 0,
        lastTime = new Date()
        me.find('input[type="file"]').bind('change',function(){
            me.file= this.files[0]
            var input = $(this)
            input.after('<div class="progress" style="display: block;width: 100%;height: 20px;background-color: black;text-align: right;"><div style="display: block;width: 0%;height: 20px; background-color: red;"></div></div>')
            input.after('<div class="speed" style="color:red;" ><span>0</span>kb/s</div>')
            chunks = Math.ceil(me.file.size / chunkSize)
            connection(input)
        }
        )
         function loadNext(input) {
                 var reader = new FileReader();
                 var start = currentChunk * chunkSize, end = start + chunkSize >= me.file.size ? me.file.size : start + chunkSize;
                     blobSlice = File.prototype.mozSlice || File.prototype.webkitSlice || File.prototype.slice
                     reader.readAsDataURL(blobSlice.call(me.file, start, end));
                 reader.onload = function(e){
                        data =e.target.result
                        me._connection.send(data)
                        num =Math.round(currentChunk / chunks * 10000) / 100.00 + "%"
                        var progress= $($(input.next('.progress')).children('div'))
                        progress.css('width',num)
                        var currTime = new Date();
                        var speed = Math.floor((chunkSize/1024)/((currTime - lastTime)/1000));
                        $($(input.next('.speed')).children('span')).html(speed)
                        lastTime = currTime
                        console.log(speed)
                    }

        }

        function connection (input){
            var endpoint =generateEndpoint()
            if (window.WebSocket) {
                    me._connection = new WebSocket(endpoint);
            }
            else if (window.MozWebSocket) {
                    me._connection = MozWebSocket(endpoint);
            }
            me._connection.onopen = function(){
                me._connection.send(JSON.stringify({"filename":me.file.name,'poolname':options.poolname,'size':me.file.size,'offset':0}))
            }
            me._connection.onmessage = function(e){
                switch(e.data){
                    case 'success':
                    if (currentChunk < chunks) {
                         currentChunk ++
                         loadNext(input);
                    }else{
                        console.log("finished loading");
                    };
                    break;
                    case 'created':
                    loadNext(input);
                    break;
                }
            }
          }
       generateEndpoint = function() {
        if (window.location.protocol == 'https:') {
            var protocol = 'wss://';
            } else {
            var protocol = 'ws://';
                }
            var host = options.ip? options.ip:window.location.host
            var endpoint = protocol + host + '/ws/rbd/';
            return endpoint;
        };

    }
})(jQuery)